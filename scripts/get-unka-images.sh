#!/bin/sh
#
# Copyright 2017, 2019-2020, 2022  Jonas Smedegaard <dr@jones.dk>
#
# This script is licensed
# under the terms of the GNU Affero General Public License,
# either version 3 of the License,
# or (at your option) any later version.
#
# Based on script by Florian Lohoff <f@zz.de>

set -eu

BASEPATH="$(dirname "$0")/.."
IMGDIR="$BASEPATH/vendor/img/unka"
DESCDIR="$BASEPATH/vendor/unka"

mkdir -p "$IMGDIR"
mkdir -p "$DESCDIR"

# TODO: maybe figure out how to resolve the more specific list
#cat unfalltypen-liste.txt | while read NO; do
for NO in $(seq 100 799); do

	TMP=$(mktemp)

	curl -s \
		--retry 10 \
		-H 'Accept: text/html,application/xhtml+xml,application/xml;q=0.9,*/*;q=0.8' \
		-H 'Accept-Encoding: gzip, deflate' \
		-H 'Accept-Language: de-DE,en;q=0.7,en-US;q=0.3' \
		-H 'Connection: keep-alive' \
		-H 'Cookie: PHPSESSID=oguvoqob8i55cdelkeofclv67q4lfd7ntiek5k86k9nk0u3q2es0; has_js=1; cookie-agreed=2' \
		-H 'DNT: 1' \
		-H 'Host: udv.de' \
		-H 'Referer: http://udv.de/unka/unka.php' \
		-H 'User-Agent: Mozilla/5.0 (X11; Linux x86_64; rv:45.0) Gecko/20100101 Firefox/45.0' \
		-H 'Content-Type: application/x-www-form-urlencoded' \
		--data "unka-inputfilternummer=$NO&submit_name1=Suchen&unka-anzzeilen1=1&unka-historyclick1=0" \
		--output "$TMP" \
		'https://udv.de/unka/unka.php'

	sleep 0.4

	curl -s \
		--retry 10 \
		-H 'Accept: */*' \
		-H 'Accept-Encoding: gzip, deflate' \
		-H 'Accept-Language: de-DE,en;q=0.7,en-US;q=0.3' \
		-H 'Connection: keep-alive' \
		-H 'Cookie: PHPSESSID=oguvoqob8i55cdelkeofclv67q4lfd7ntiek5k86k9nk0u3q2es0; has_js=1; cookie-agreed=2' \
		-H 'DNT: 1' \
		-H 'Host: udv.de' \
		-H 'Referer: http://udv.de/unka/unka.php' \
		-H 'User-Agent: Mozilla/5.0 (X11; Linux x86_64; rv:45.0) Gecko/20100101 Firefox/45.0' \
		-H 'X-Requested-With: XMLHttpRequest' \
		--output "$TMP" \
		'https://udv.de/unka/unka/unka_suche.php?keyword=linein'

	# <img src="images/unfalltypen/small/70.png" width="90" height="130" alt="" />
	imgpaths=$(sed -ne '/img src/ { s/^.*img src..//; s/. width=.*$//; p }' < "$TMP")
	# unkatyp_onclick(4, 'ut4', '4')
	utypes=$(sed -ne '/unkatyp_onclick/ { s/^.*unkatyp_onclick.//; s/,.*$//; p }' < "$TMP")

	if [ -z "$utypes" ]; then
		continue
	fi

	for utype in $utypes; do
		curl -s \
			--retry 10 \
			-H 'Accept: */*' \
			-H 'Accept-Encoding: gzip, deflate' \
			-H 'Accept-Language: de-DE,en;q=0.7,en-US;q=0.3' \
			-H 'Connection: keep-alive' \
			-H 'Cookie: PHPSESSID=oguvoqob8i55cdelkeofclv67q4lfd7ntiek5k86k9nk0u3q2es0; has_js=1; cookie-agreed=2' \
			-H 'DNT: 1' \
			-H 'Host: udv.de' \
			-H 'Referer: http://udv.de/unka/unka.php' \
			-H 'User-Agent: Mozilla/5.0 (X11; Linux x86_64; rv:45.0) Gecko/20100101 Firefox/45.0' \
			-H 'X-Requested-With: XMLHttpRequest' \
			--output "$DESCDIR/$NO-$utype.html" \
			"https://udv.de/unka/unka/unka_detail.php?unka_nr=$utype&keyword=linein"
	done

	echo "$imgpaths"
	for imgpath in $imgpaths; do
		imgname=$(basename "$imgpath")
		curl -s \
			--retry 10 \
			--remote-time \
			--output "$IMGDIR/$imgname" \
			"https://udv.de/unka/$imgpath"
		ln -fs "$imgname" "$IMGDIR/cause$NO.png"
	done

	rm "$TMP"

	sleep 1
done
